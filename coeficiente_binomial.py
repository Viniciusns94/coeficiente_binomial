#formula (n k) = n!/( k! * (n-k)!)

def fatorial(x):
	if(x < 0):
		print("Erro não existe fatorial de numero negativo");
		return 0;
	else:
		if(x == 0):
			return 1
		else:
			num = x-1
			while(num >= 1):
				x *= num  
				num -= 1
		return x

def numero_binomial(n, k):
	return fatorial(n) / ( fatorial(k) * fatorial(n - k) )

n = int(input("Digite o valor de n: "))
k = int(input("Digite o valor de k: "))

while(k > n):
	print("ERRO!\nk deve ser menor ou igual a n = ", n)
	k = int(input("\nDigite o valor de k valido: "))

print("Número binomial: ",numero_binomial(n, k))